#!/bin/bash
timestamp=`date +%Y/%m/%d-%H:%M:%S`
echo "System path is $PATH at $timestamp"
cd /functions_data/Model
python3 ai_model_deployment_on_edge_device.py
echo "######### END ##########"
